import './styles/app.scss';
import 'bootstrap';
import './styles/style-aos.scss';
import './styles/style-type.scss';
import './styles/style-popup.scss';
import './styles/style-masonry.scss';
import './styles/style-tilt.scss';
import './styles/style-slick.scss';
import './styles/style-lightbox.scss';