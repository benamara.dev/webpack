import "magnific-popup";

$('.gallery').magnificPopup({
    delegate: '.popimg ',
    type:'image',
    
    gallery:{
        enabled: true, // set to true to enable gallery
        navigateByImgClick: true,
        tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
    
    },
    zoom: {
        enabled: true,
        duration: 1000,
    },
    
    closeBtnInside: false,
    closeOnBgClick: true,
    showCloseBtn: true,
    enableEscapeKey : true,
    
 
});
