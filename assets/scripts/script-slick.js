//import slick from 'slick-carousel';
import "slick-carousel";


$('.custom-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1000,
    centerMode: true,
    centerPadding: '70px',
   // rows: 0,
    arrows: true,
   prevArrow: '<span class="slick-prev"><</span>',
   nextArrow: '<span class="slick-next">></span>',
  // fade: true,
  adaptiveHeight: true,





   responsive: [
      {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
      {
          breakpoint: 900,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
      {
          breakpoint: 550,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        }
      ]
});

/* */
